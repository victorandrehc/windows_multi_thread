// thread_1.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <windows.h>
#include <stdio.h>
#include <pthread.h>
#include <errno.h>
#include <signal.h>
#include <iostream>
using namespace std;
#define n 5

int values[][n] = {
        {1 ,2 ,3 ,4 ,5},
        {10 ,9 ,8 ,7 ,6 },
        {50, 3 ,5 ,7 ,9},
        {1 ,4 ,7 ,2 ,5 },
        {10, 9, 6, 3, 2},
    };

int cmpfunc (const void * a, const void * b)
{
   return ( *(int*)a - *(int*)b );
}

void *ordenate(void* i){
	int i_int=(int)i;
	cout<<"thread on going "<<i_int<<endl;
	qsort(values[i_int], n, sizeof(int), cmpfunc);
	return NULL;
}

int _tmain(int argc, _TCHAR* argv[])
{
	pthread_t thread[n];
	for(int i=0;i<n;i++){
		for(int j=0;j<n;j++){
			cout<<values[i][j]<<";";
		}
		cout<<endl;
	}
	cout<<endl;
	for(int i=0;i<n;i++){
		pthread_create(&thread[i],NULL,ordenate,(void*) i);
	}
	for(int i=0;i<n;i++){
		pthread_join(thread[i],NULL);
	}
	
	cout<<endl;
	for(int i=0;i<n;i++){
		for(int j=0;j<n;j++){
			cout<<values[i][j]<<";";
		}
		cout<<endl;
	}
	
	
	system("pause");
	return 0;
}

