// semaforo.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <windows.h>
#include <stdio.h>
#include <pthread.h>
#include <semaphore.h>
#include <conio.h>
#include <errno.h>
#include <iostream>
using namespace std;

#define NTHREAD 50
long cont=0;
int tecla=0;
sem_t semaforo;

void *incrementa(void* arg){
	int id=(int)arg;
	while(tecla!=1){	
		sem_wait(&semaforo);
		for(int i=0;i<1000000;i++){
			cont++;
		} 
		printf("thread %i cont %i\n",id,cont);
		sem_post(&semaforo);
	}
	return NULL;
}
int _tmain(int argc, _TCHAR* argv[])
{
	pthread_t thread[NTHREAD]; 
	sem_init(&semaforo,0,1);

	for(int i=0;i<NTHREAD;i++){
		pthread_create(&thread[i],NULL,incrementa,(void*) i);
	}
	while(tecla!=1){
		scanf("%i",&tecla);
	}
	for(int i=0;i<NTHREAD;i++){
		pthread_join(thread[i],NULL);
	}
	sem_destroy(&semaforo);
	system("pause");
	return 0;
}

