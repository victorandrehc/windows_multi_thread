// reserva_avioes_carteiras.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <windows.h>
#include <stdio.h>
#include <pthread.h>
#include <conio.h>
#include <errno.h>
#include <iostream>
using namespace std;

#define NTHREAD 20
#define NLUGARES 6

int *lugares;

pthread_mutex_t mutex;
pthread_mutexattr_t mutexattr;

void imprime_lugares(int * lugares){
	cout<<"mapa de lugares\n-1->Livre\n1->ocupado"<<endl;
	for(int i=0;i<NLUGARES;i++){
		cout<<lugares[i]<<"; "; 
	}
	cout<<"\n";
}

void * reserva(void* arg){
	int id=(int)arg;
	int lugar=id%NLUGARES;
	int status=pthread_mutex_lock(&mutex);
	if (status !=0){
		  if (status == EDEADLK)
			  printf ("Erro EDEADLK na conquista do Mutex!\n");
		  else
			  printf ("Erro inesperado na conquista do Mutex! Codigo = \n", status);
		  exit(0);
	}
	if(lugares[lugar]==-1){
		lugares[lugar]=id;
		cout<<"thread "<<id<<"retorna SUCESSO no lugar"<<lugar<<endl;
	}else{
		cout<<"thread "<<id<<"retorna FALHA no lugar"<<lugar<<endl;
	}
	status=pthread_mutex_unlock(&mutex);
	if (status !=0){
		  if (status == EPERM) 
			  printf ("Erro: tentativa de liberar mutex nao-conquistado!\n");
		  else
			  printf ("Erro inesperado na liberacao do Mutex! Codigo = \n", status);
		  exit(0);
	}
	return NULL;
}
int _tmain(int argc, _TCHAR* argv[])
{
	pthread_t threads[NTHREAD];
	lugares=(int*)malloc(sizeof(int)*NLUGARES); //inicialização do vetor lugares
	for(int i=0;i<NLUGARES;i++){
		lugares[i]=-1;
	}
	imprime_lugares(lugares);
	
	//cria mutex
	pthread_mutexattr_init(&mutexattr);
	if (pthread_mutexattr_settype(&mutexattr, PTHREAD_MUTEX_ERRORCHECK)){
		perror("erro em pthread_mutexattr_settype\n");
	}
	if(pthread_mutex_init(&mutex,&mutexattr)){
		perror("erro em pthread_mutex_init\n"); 
	}

	for(int i=0;i<NTHREAD;i++){
		//cout<<"criando thread"<<i<<endl;
		pthread_create(&threads[i],NULL,reserva,(void*)i);
	}
	
	for(int i=0;i<NTHREAD;i++){
		pthread_join(threads[i],NULL);
	}
	imprime_lugares(lugares);
	pthread_mutex_destroy(&mutex);
	free(lugares);
	system("pause");
	return 0;
}

