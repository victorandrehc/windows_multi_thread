// jantar_filosofos.cpp : Defines the entry point for the console application.
//


#include "stdafx.h"
#include <windows.h>
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <conio.h>
#include <errno.h>
#include <semaphore.h>

//Defini��o de constantes a serem utilizadas
#define N 5
#define ESQUERDA (i+N-1)%N
#define DIREITA (i+1)%N
#define PENSANDO 0
#define FAMINTO 1
#define COMENDO 2

//defini��o de variaveis globais
//sem_t semaforo;
int estado[N];
int tecla=0;
sem_t mutex;
sem_t s[N];

pthread_mutex_t print;
pthread_mutexattr_t Attrprint;


//fun��es utilizadas
void pensa(int i){
	pthread_mutex_lock(&print);
	printf("Filosofo %i pensando\n",i);
	pthread_mutex_unlock(&print);
	Sleep(2000);
}

void come(int i){
	pthread_mutex_lock(&print);
	printf("Filosofo %i comendo\n",i);
	pthread_mutex_unlock(&print);
	Sleep(3000);
}
void test(int i){
	if(estado[i]==FAMINTO && estado[ESQUERDA]!=COMENDO && estado[DIREITA]!=COMENDO){
		estado[i]=COMENDO;
		sem_post(&s[i]);
	}
}
void conquista_talheres(int i){
	sem_wait(&mutex);
	estado[i]=FAMINTO;
	test(i);
	sem_post(&mutex);
	sem_wait(&s[i]);
}
void devolve_talheres(int i){
	sem_wait(&mutex);
	estado[i]=PENSANDO;
	test(ESQUERDA);
	test(DIREITA);
	sem_post(&mutex);
}

void* filosofo(void* arg){//thread
	int i=(int)arg;
	while(tecla==0){
		pensa(i);
		conquista_talheres(i);
		come(i);
		devolve_talheres(i);
	}
	return NULL;
}

//c�digo principal
int _tmain(int argc, _TCHAR* argv[]){
	
	//cria��o das threads
	pthread_t thread[N]; 
	sem_init(&mutex,0,1);

	pthread_mutexattr_init(&Attrprint);
	pthread_mutexattr_settype(&Attrprint,PTHREAD_MUTEX_ERRORCHECK);
	pthread_mutex_init(&print,&Attrprint);
	
	for(int i=0; i<N;i++){
		sem_init(&s[i],0,0);
	}	
	for(int i=0; i<N;i++){
		pthread_create(&thread[i],NULL,filosofo,(void*) i);
	}
	//espera da condi��o de saida
	while(tecla==0){
		scanf("%i",&tecla);
	}
	//destrui��o das threads
	for(int i=0;i<N;i++){
		pthread_join(thread[i],NULL);
	}
	sem_destroy(&mutex);
	for(int i=0; i<N;i++){
		sem_destroy(&s[i]);
	}
	pthread_mutex_destroy(&print);
	pthread_mutexattr_destroy(&Attrprint);

	system("pause");
	return 0;
}

